<?php
/*
 * Allows users to like other users word clouds. Each user can like a particular word cloud once, but not their own! 
 *
 * Expects:
 *     word_cloud_id(int) - The word cloud ID the current user wants to like.
 *
 * Outputs: Either an empty success response or some error on failure.
 *          
 */
require_once('app/header.php');
require_once('app/user.php');
require_once('app/wordcloud.php');

$user = new User();
$user->confirmSignedOn();
$wordcloud = new WordCloud();

try {
    $wordcloud->like($_POST['word_cloud_id']);
} catch (Exception $e) {
    jsonClientError($e->getMessage(), 'Could not like this word cloud.');
}

jsonOK();
