<?php

/*
INPUTS EXPECTED:

    username - The username supplied on login form.
    password - The password supplied on login form.
    
    
JSON RESPONSE:
    {
        status: OK | ERROR
        [message]: string
    }
*/

require_once('app/header.php');

if($_GET['do'] == 'logout') {
    unset($_SESSION['user_id']);
    jsonOK();
}

if(isset($_SESSION['user_id'])) {
    pg_prepare($dbconn, "get_user", 'SELECT * FROM users WHERE id=$1');
    $result = pg_fetch_array(pg_execute($dbconn, "get_user", array($_SESSION['user_id'])), NULL, PGSQL_ASSOC);
    
    if($result)
        jsonOK(publicUserData($result));
}

$username = strtolower($_POST['username']);
$password = $_POST['password'];

//Check username is not empty.
if(empty($username))
    jsonClientError('USERNAME_EMPTY', 'You must enter a username.');

//Check password is not empty.
if(empty($password))
    jsonClientError('PASSWORD_EMPTY', 'You must enter a password.');

pg_prepare($dbconn, "get_user", 'SELECT * FROM users WHERE username=$1');
    $result = pg_fetch_array(pg_execute($dbconn, "get_user", array($username)), NULL, PGSQL_ASSOC);
    
if(crypt($password, $result['password']) == $result['password']) {
    $_SESSION['user_id'] = $result['id'];
    jsonOK(publicUserData($result));
} else {
    jsonClientError('LOGIN_ERROR', 'The username/password are invalid.');
}

jsonOK();

