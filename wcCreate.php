<?php

/*
 * Creates a word cloud for the user given some keyword and type. 
 *
 * Expects:
 *     type(string) - Value can be either "WORD" or "USER":
 *                      For "WORD", results are based on the words contained in the target tweets. 
 *                      For "USER", results are based on the words found in the tweets made by some target user.
 *
 *     keyword(string) - The target value to search twitter. Value should be alphanumeric only.
 *
 * Outputs(JSON):
 *      id(int):                Unique ID of the word cloud.
 *      user	                The creators user profile.
 *      type("WORD" | "USER")   The type provided by the user denoting how word cloud was generated.
 *      keyword(string):        The keyword provided by the user to generate this word cloud.
 *      likes(int):             The number of times this word cloud has been liked by others. Defaults to 0.
 *      has_liked(bool):       Boolean value if user has liked this word cloud already.
 *      shared(bool):          Boolean value if user has shared this word cloud with other users. Defaults to 0.    
 *      words(array): [word(string): word_count(int), ... ]
 *          
 */
require_once('app/header.php');
require_once('app/user.php');
require_once('app/wordcloud.php');

$user = new User();
$wordcloud = new WordCloud();
$user->confirmSignedOn();

$keyword = $_POST['keyword'];
$type = $_POST['type'] == "WORD" ? "WORD" : "USER";

if(empty($keyword))
    jsonClientError('KEYWORD_EMPTY', 'You must enter a keyword.');

$urlprefix = '';
if($type == 'USER')
    $urlprefix = 'from:';

try {
    $tweets = json_decode($twitter->setGetfield('?q=' . urlencode($urlprefix . $keyword))
        ->buildOauth('https://api.twitter.com/1.1/search/tweets.json', 'GET')
        ->performRequest(), true);
        
} catch (Exception $e) {
    jsonClientError('TWITTER_ERROR', 'We could not connect to twitter, please try again in a few moments.' . $e->getMessage());
}

$stopwords = array("a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "i", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "rt", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the");



$word_array = array();
foreach($tweets['statuses'] as $tweet) {
    $text = preg_replace("@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@", '', $tweet['text']); //Remove all links
    $text = preg_replace("/[^A-Za-z0-9@# ]/", '', $text); //Remove any characters that are not alphanum or a space
    $text = strtoupper($text);
    
    $words = explode(" ", $text);
    foreach($words as $word) {
        
        $firstLtr = substr($word, 0, 1);
        if(
            in_array(strtolower($word), $stopwords) ||
            $word == "" ||
            //!ctype_alpha($word) ||
            $firstLtr == '@' ||
            $firstLtr == '#'
        )
            continue;
        
        if(array_key_exists($word, $word_array))
            $word_array[$word] += 1;
        else
            $word_array[$word] = 1;
    }
}

arsort($word_array, SORT_NUMERIC);
$word_array = array_slice($word_array, 0, 20);
$currUser = $user->getCurrentUser();
$keyword = preg_replace("/[^A-Za-z0-9 ]/", '', $keyword);

pg_query_params($dbconn, "DELETE FROM wordclouds WHERE user_id=$1 and shared=$2", array($currUser['id'], 0));
pg_prepare($dbconn, "create_word_cloud", 'INSERT INTO wordclouds (user_id, likes, type, keyword, shared, word_data) VALUES ($1, $2, $3, $4, $5, $6)');
pg_execute($dbconn, "create_word_cloud", array($currUser['id'], 0, $type, $keyword, 0, serialize($word_array)));


jsonOK($wordcloud->getLast($currUser['id']));

