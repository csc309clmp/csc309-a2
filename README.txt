Contributor:	UTorID:
Charles Li	licharl2
Miles Petrov	petrovmi

Live version can be found at: https://cs.utm.utoronto.ca/~licharl2/CSC309A2/

INSTRUCTIONS:
1. Edit database credentials in app/header.php as appropriate
2. Run app/install.php (on a browser) to instantiate database tables

Requirements NOT met:
- No predefined images to select
- word clouds not properly styled
- back-end does not store generated word clouds as html+css => front-end must regenerate word cloud without guarantee of same result between gen in form#create and gen in li#main
- can not undo (take back) like
- not properly tested, may have some bugs

Additional features:
- store a word cloud before sharing, so when user leaves and returns, it's the same word cloud (same words and sizes), though maybe in a different style
- allow user to log out

-----------------------
Tasks

Work Divvy: Front-end & Back-end

Front-end: Charles
Back-end: Miles

Progress:
- Back-end done
- Front-end maybe done

TODO:
- test it out
