/**
 * major views: div#login, div#signup, div#main
 * 
**/

var az = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
var num = ['0','1','2','3','4','5','6','7','8','9'];
var sym = ['_'];
var MAX_SIZE = 5;

function showView(view){
	console.log("call showView('"+view+"')");
	$("body > div").hide();
	$(view).show();
}

function showError(error){
	error = typeof error === undefined ? "Oops, something went wrong." : error;
	console.log("call showError('"+error+"')");
	if(error == ""){
		error = "Oops, something went wrong.";
	}
	$("div#error > div.error").html(error);
	$("body > div#error").show();
}

/* 
function validLogin(user, pass){
	console.log("call validLogin('"+user+"', '"+pass+"')");
	var valid = true;
	var userArr = user.split('');
	// the first character of the username is not an alphabet
	if(az.indexOf(userArr[0].toLowerCase()) == -1){
		$("div.error#name").show();
		valid = false;
	}
	// check if each character of the username passes the syntax test
	for(var c in userArr){
		if(!(c.toLowerCase() in az || c in num || c in sym)){
			valid = false;
		}
	}
	// the password is less than 6 characters long
	if(pass.length < 6){
		$("div.error#pass").show();
		valid = false;
	}
	return valid;
}
*/

// check if user already has logged in
function loggedIn(){
	console.log("call loggedIn()");
	// Ajax request goes here...
	$.ajax({
		url: "login.php",
		type: "GET",
		success: function(){
			return true;
		},
		error: function(){
			return false;
		}
	});
}

// take input values and send a login request to login.php
function login(){
	console.log("call login()");
	var view = "body > div#login";
	var user = $('form#login input#username').val();
	var pass = $('form#login input#password').val();
	// Ajax request goes here...
	$.ajax({
		async: false,
		url: "login.php",
		type: "POST",
		data: {username: user, password: pass},
		success: function(resp){
			//expected response: {user_id: int, username: string, profile_img: string}
			view = "body > div#main";
		},
		error: function(resp){
			//expected response: {code: string, message: string}
			view = "body > div#login";
			$("div.error#login").show();
			showError(resp["message"]);
		}
	});
	return view;
}

function validImage(file){
	console.log("call validImage('"+file+"')");
	var valid = true;
	var type = file.type;
	var size = file.size;
	//if the file type is not an image
	if(type.indexOf("image") != 0){
		valid = false;
		$("div.error#type").show();
	}
	//if the file size is more than 50KB
	if(size > 51200){
		valid = false;
		$("div.error#size").show();
	}
	return valid;
}

// Assuming the file has already been validated
function previewImage(file){
	console.log("call previewImage('"+file+"')");
	var reader = new FileReader();
	reader.onload = function (e) {
        $("form#signup img#preview").attr("src", e.target.result);
    }
    reader.readAsDataURL(file);
}

function signup(){
	console.log("call signup()");
	var view = "body > div#signup";
	var user = $('form#signup input#username').val();
	var pass = $('form#signup input#password').val();
	// var file = $('form#signup')[0].files[0];
	var fd = new FormData($('form#signup')[0]);
	// Ajax request goes here...
	$.ajax({
		async: false,
		url: "register.php",
		type: "POST",
		// username: user,
		// password: pass,
		//sends keys: {username, password, profile_img ({name, type, size})}
		data: fd,
		contentType: false,
		// cache: false,
		processData: false,
		success: function(resp){
			//expected response: {user_id: int, username: string, profile_img: string}
			console.log("SIGNUP SUCCESS");
			view = "body > div#main";
		},
		error: function(resp){
			//expected response: {code: string, message: string}
			console.log("SIGNUP ERROR");
			view = "body > div#signup";
			$("div.error#signup").show();
			showError(resp["message"]);
		}
	});
	console.log("returning view: "+view);
	return view;
}

function create(type){
	console.log("call create('"+type+"')");
	var keyword = $("form#create input#entry").value;
	// Ajax request goes here...
	$.ajax({
		async: false,
		url: "wcCreate.php",
		type: "POST",
		data: {keyword: keyword, type: type},
		success: function(resp){
			//expected response: {wc_id, user_id, type, keyword, likes, has_liked, shared, words[]}
			//success means we take the array of words and put it in html
			$("form#create div.gen").id = resp["id"];
			var gen = "";
			var words = resp["words"]; //a dictionary of {word: word_count} pairs
			var i = 0;
			for(var word in words){
				var size = words[word] / 2;
					if(size > MAX_SIZE){
						size = MAX_SIZE;
					}
				gen += "<span class='gen' id='"+i+"' style='font-size: "+size+"em;'>"+word+"</span>";
				i += 1;
			}
			$("form#create div.gen").html(gen);
			return resp["id"];
		},
		error: function(resp){
			//expected response: {code: string, message: string}
			$("div.error#generate").show();
			showError(resp["message"]);
		}
	});
}

function share(){
	console.log("call share()");
	var id = $("form#create div.gen").id;
	// Ajax request goes here...
	$.ajax({
		async: false,
		url: "wcShare.php",
		type: "POST",
		data: {word_cloud_id: id},
		success: function(resp){
			//expected response: {}
			//success means backend updated the wordcloud to be visible to the public
			$("div.success#share").show();
		},
		error: function(resp){
			//expected response: {code: string, message: string}
			$("div.error#share").show();
			showError(resp["message"]);
		}
	});
}

function sortBy(order, start, stop){
	order = typeof order === undefined ? "TIME" : order;
	start = typeof start === undefined ? 0 : start;
	stop = typeof stop === undefined ? 10 : stop;
	console.log("call sortBy('"+order+"', "+start+", "+stop+")");
	$("form#main input#more").hide();
	// Ajax request goes here...
	$.ajax({
		async: false,
		url: "wcView.php",
		type: "POST",
		data: {sort_by: order, pageinate_start: start, pageinate_stop: stop}
		success: function(resp){
			//expected response: list of word_cloud dictionaries
			// {
			//	wordclouds: [{wc_id, user={id, username, profile_image}, type, keyword, likes, has_liked, shared, words={word:count, ...} }, ...],
			// }
			//success means we take the array of wordclouds and insert their info in html
			var wordclouds = resp["wordclouds"];
			var post = "";
			var i = 0;
			for(var wc in wordclouds){
				var id = wc["id"];
				var user = wc["user"];
				var type = "";
				switch(wc["type"]){
					case "WORD":
						type = "the keyword(s) ";
						break;
					case "USER":
						type = "tweets by ";
						break;
					default:
						//do nothing
				}
				post += "<li class='main' id='"+id+"''>";
				post += "<div id='"+user["id"]+"'>"+user["username"]+"</div>";
				post += "<img class='main' id='"+user["id"]+"' height='64' width='64' alt='"+user["username"]+"' src='"+user["profile_image"]+"'/>";
				post += "<p>I just generated a Tweet Cloud about "+type+"<i>"+wc["keyword"]+"</i>!</p>";
				post += "<div class='gen' id='"+id+"'>";
				var words = wc["words"]; //a dictionary of {word: word_count} pairs
				var gen = "";
				var j = 0;
				for(var word in words){
					var size = words[word] / 2;
					if(size > MAX_SIZE){
						size = MAX_SIZE;
					}
					gen += "<span class='gen' id='"+j+"' style='font-size: "+size+"em;'>"+word+"</span>";
					j += 1;
				}
				post += gen + "</div>";
				post += "Like? <input class='like' id='"+id+"' type='checkbox' name='like'";
				//if user liked it already, check it, otherwise don't check it
				if(wc["has_liked"]){
					post += " checked";
				}
				post += "/>";
				post += "<span class='likes' id='"+id+"'>"+wc["likes"]+"</span> Likes";
				post += "</li>";
				i += 1;
			}
			//there might be more to show after stop
			if(i >= stop){
				$("form#main input#more").show();
			}
			return post;
		},
		error: function(resp){
			//expected response: {code: string, message: string}
			$("div.error#sort").show();
			showError(resp["message"]);
		}
	});
}

function like(id, checked){
	checked = typeof checked === undefined ? true : checked;
	console.log("call like('"+id+"', "+checked+")");
	// Ajax request goes here...
	$.ajax({
		url: "wcLike.php",
		type: "POST",
		data: {word_cloud_id: id, checked: checked},
		success: function(resp){
			//expected response: {}
			//success means backend added 1 to the likes of the wordcloud
			//do nothing
		},
		error: function(resp){
			//expected response: {code: string, message: string}
			$("div.error#like").show();
			showError(resp["message"]);
		}
	});
}

// take input values and send a logout request to login.php
function logout(){
	console.log("call logout()");
	var view = "body > div#main";
	// Ajax request goes here...
	$.ajax({
		url: "login.php",
		type: "GET",
		data: {do: "logout"},
		success: function(resp){
			view = "body > div#login";
		},
		error: function(resp){
			view = "body > div#main";
			$("div.error#logout").show();
			showError(resp["message"]);
		}
	});
	return view;
}

// This is executed when the document is ready (the DOM for this document is loaded)
$(function(){
	console.log("document is ready");
	//hide all major views
	$("body > div").hide();
	//hide all error divs
	$("div.error").hide();
	//hide all success divs
	$("div.success").hide();
	
	//determine the page to view first
	//default is login page
	var view = "body > div#login";
	if(loggedIn()){
		view = "body > div#main";
	}
	console.log("show initial view: "+view);
	showView(view);

	// Setup all events here and display the appropriate UI

	// login button in login form was clicked
	$("form#login input#login").on("click", function(){
		console.log("login button in login form was clicked");
		$("div.error").hide();
		$("div#error").hide();
		$("div.success").hide();
		// if(validLogin()){
			view = login();
		/*
		} else {
			view = "body>div#login";
		}
		*/
		showView(view);
	});

	// signup button in login form was clicked
	$("form#login input#signup").on("click", function(){
		console.log("signup button in login form was clicked");
		$("div.error").hide();
		$("div#error").hide();
		$("div.success").hide();
		view = "body > div#signup";
		showView(view);
	});

	// a file from clicking upload image button in signup form was selected
	$("form#signup input#upload").on("change", function(){
		console.log("a file from clicking upload image button in signup form was selected");
		//each file has: name, type, size, lastModifiedDate
		var files = this.files;
		//check for 'files' support and has at least one file
		if(files && files[0]){
			var file = files[0];
			if(validImage(file)){
				previewImage(file);
			}
		} else {
			$("div.error#upload").show();
		}
	});

	/*
	// an image in the form of a button of signup form was clicked
	$("form#signup input.predef").click(function(){
		console.log("an image in the form of a button of signup form was clicked");
		var src = $(this).attr("src");
		$("img.signup#preview").attr("src", src);
	});
	*/
	
	// signup button in signup form was clicked
	$("form#signup input#signup").on("click", function(){
		console.log("signup button in signup form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		view = "body > div#signup";
		view = signup();
		/*
		//if user selected upload radio
		if($("form#signup radio#upload").checked){
			view = signup(form);
		}
		//else if user selected predef radio
		else if($("form#signup radio#predef").checked){
			
			view = signup(form);
		}
		//else no selection
		else{
			$("div.error#null").show();
		}
		*/		
		showView(view);
	});

	// generate by keyword button in create form was clicked
	$("form#create input#keyword").on("click", function(){
		console.log("generate by keyword button in create form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		create("WORD");
	});

	// generate by handle button in create form was clicked
	$("form#create input#handle").on("click", function(){
		console.log("generate by handle button in create form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		create("USER");
	});

	// share button in create form was clicked
	$("form#create input#share").on("click", function(){
		console.log("share button in create form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		var id = $("form#create div.gen").id;
		share();
	});

	var start = 0;
	var stop = 10;
	var order = "";
	// latest button in sort form was clicked
	$("form#sort input#latest").on("click", function(){
		console.log("latest button in sort form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		start = 0;
		stop = 10;
		order = "TIME";
		var html = sortBy(order, start, stop);
		$("form#main ol#content").html(html);
	});

	// popular button in sort form was clicked
	$("form#sort input#popular").on("click", function(){
		console.log("popular button in sort form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		start = 0;
		stop = 10;
		order = "LIKES";
		var html = sortBy(order, start, stop);
		$("form#main ol#content").html(html);
	});

	// like checkbox in main form was clicked
	$("form#main input.like").on("click", function(){
		console.log("like checkbox in main form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		like(this.id, this.checked);
	});

	// more button in main form was clicked
	$("form#main input#more").on("click", function(){
		console.log("more button in main form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		start += 10;
		stop += 10;
		var html = sortBy(order, start, stop);
		$("form#main ol#content").append(html);
	});

	// logout button in logout form was clicked
	$("form#logout input#logout").on("click", function(){
		console.log("logout button in logout form was clicked");
		$("div#error").hide();
		$("div.error").hide();
		$("div.success").hide();
		view = logout();
	});
});
