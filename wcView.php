<?php
/*
 * Returns a list of word clouds that belong to the user or are shared by other users. 
 *
 * Expects:
 *      sort_by(LIKES | TIME)     - Sort by number of likes or most recent.
 *
 *      show_own(bool)            - Optional. Display only users own word clouds.
 *
 *      pageinate_start(int)      - Optional. Defaults to 0, returns all word clouds starting after this number.
 *
 *      pageinate_stop(int)       - Optional. Defaults to 10, returns all word clouds before this number.
 *
 * Outputs(JSON):
 *      word_clouds: [
 *          id(int):                Unique ID of the word cloud.
 *          user_id                 The creators user ID.
 *          type("WORD" | "USER")   The type provided by the user denoting how word cloud was generated.
 *          keyword(string):        The keyword provided by the user to generate this word cloud.
 *          likes(int):             The number of times this word cloud has been liked by others. Defaults to 0.
 *          has_liked(bool):        Boolean value if user has liked this word cloud already.
 *          shared(bool):           Boolean value if user has shared this word cloud with other users. Defaults to 0.    
 *          words(array):           [word(string): word_count(int), ... ]
 *      , ... ]
 *          
 */
require_once('app/header.php');
require_once('app/user.php');
require_once('app/wordcloud.php');

$user = new User();
$wordcloud = new WordCloud();
$user->confirmSignedOn();

$sort_by = $_POST['sort_by'] == "TIME" ? "TIME" : "LIKES";
$show_own = $_POST['show_own'] | False;
$pageinate_start = $_POST['pageinate_start'] < 1 ? 0 : $_POST['pageinate_start'];
$pageinate_stop = $_POST['pageinate_stop'] < 1 ? 10 : $_POST['pageinate_stop'];

$limit = $pageinate_stop - $pageinate_start;

jsonOK($wordcloud->viewList($sort_by, $limit, $pageinate_start, $show_own));

