<?php
require_once('header.php');
require_once('user.php');

class WordCloud {
    
    private $db;
    private $user;
    
    public function __construct() {
        global $dbconn;
        $this->db = $dbconn;
        $this->user = new User();
        $this->currentUser = $this->user->getCurrentUser();
    }
    
    
    private function output($data) {
        
        $data['has_liked'] = pg_num_rows(pg_query_params($this->db, "SELECT * FROM wordcloudlikes WHERE user_id=$1 and wordcloud_id=$2", array($this->currentUser['id'], $data['id']))) > 0 ? True : False;

	$data['user'] = $this->user->byID($data['user_id']);
	unset($data['user_id']);

        $data['shared'] = $data['shared'] == "t" ? True : False;
        $data['words'] = unserialize($data['word_data']);
        unset($data['word_data']);
        
        return $data;
    }
    
    
   /*
    * Returns last word cloud by the given user.
    * 
    * Parameters:
    *   $user_id - The ID of the user who created the word cloud.
    */
    public function getLast($user_id) {
        $wc = pg_fetch_array(pg_query_params($this->db, "SELECT * FROM wordclouds WHERE user_id=$1 ORDER BY ID DESC LIMIT 1", array($user_id)), NULL, PGSQL_ASSOC);
        return $this->output($wc);
    }
    
    /*
    * Returns word cloud with given ID.
    * 
    * Parameters:
    *   $ID - The ID of the word cloud.
    */
    public function getWCByID($ID) {
        $wc = pg_fetch_array(pg_query_params($this->db, "SELECT * FROM wordclouds WHERE id=$1", array($ID)), NULL, PGSQL_ASSOC);
        return $this->output($wc);
    }
    
    
   /*
    * Returns a list of word clouds.
    */
    public function viewList($sort_by, $limit, $offset, $user_flag) {
        
        $query = 'SELECT * FROM wordclouds WHERE shared=\'1\' OR user_id=' . $this->currentUser['id'] . ' ';
        
        if($user_flag)
            $query = 'SELECT * FROM wordclouds WHERE user_id=' . $this->currentUser['id'] . ' ';
        
        if($sort_by == "LIKES")
            $query = $query . 'ORDER BY likes DESC ';
        else
            $query = $query . 'ORDER BY id DESC ';
        
        $query = $query . 'LIMIT ' . $limit . ' OFFSET ' . $offset;
        
        $results = array();
        foreach(pg_fetch_all(pg_query($this->db, $query)) as $r)
            $results[] = $this->output($r);
        
        return $results;
    }
    
    
    /*
    * Allows the current user to like a particular word cloud.
    * 
    * Parameters:
    *   $wc_ID - The ID of the word cloud.
    */
    public function like($wc_ID) {
        
        if($wc_ID <= 0)
            throw new Exception('INVALID_ID');
        
        $wc = $this->getWCByID($wc_ID);
        
        if($wc['user_id'] == $this->currentUser['id'])
            throw new Exception('LIKES_OWN_WC');
        
        $already_liked = pg_num_rows(pg_query_params($this->db, "SELECT * FROM wordcloudlikes WHERE user_id=$1 and wordcloud_id=$2", array($this->currentUser['id'], $wc_ID)));
        if($already_liked > 0)
            throw new Exception('LIKED_ALREADY');
        
        pg_query_params($this->db, "INSERT INTO wordcloudlikes (user_id, wordcloud_id) VALUES ($1, $2)", array($this->currentUser['id'], $wc_ID));
        pg_query_params($this->db, "UPDATE wordclouds SET likes=$1 WHERE id=$2", array($wc['likes'] + 1, $wc_ID));
    }
    
    
    /*
    * Allows the current user to share a particular word cloud.
    * 
    * Parameters:
    *   $wc_ID - The ID of the word cloud.
    */
    public function share($wc_ID) {
        
        if($wc_ID <= 0)
            throw new Exception('INVALID_ID');
        
        pg_query_params($this->db, "UPDATE wordclouds SET shared=$1 WHERE id=$2", array(True, $wc_ID));
    }
}
