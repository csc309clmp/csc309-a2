<?php
require_once('header.php');

pg_query($dbconn, "
    DROP TABLE IF EXISTS users;
    CREATE TABLE users(
        id              SERIAL,
        username        varchar(255) NOT NULL,
        password        varchar(255) NOT NULL,
        profile_image   varchar(255) NOT NULL
    );
    
    DROP TABLE IF EXISTS wordclouds;
    CREATE TABLE wordclouds(
        id         SERIAL,
        user_id    int NOT NULL,
        likes      int NOT NULL,
        type       varchar(255) NOT NULL,
        keyword    varchar(255) NOT NULL,
        shared     boolean NOT NULL,
        word_data  text NOT NULL
    );
    
    DROP TABLE IF EXISTS wordcloudlikes;
    CREATE TABLE wordcloudlikes(
        user_id            int NOT NULL,
        wordcloud_id       int NOT NULL
    );
");
