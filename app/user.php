<?php
require_once('header.php');

class User {
    
    private $user = False;
    private $db;
    
    public function __construct() {
        global $dbconn;
        $this->db = $dbconn;
    }
    
    
   /*
    * Returns whether the current user has been authenticated (logged in).
    * 
    * @return boolean
    */
    public function isSignedOn() {
        return $this->getCurrentUser() ? True : False;
    }

    /*
    * Returns whether the current user has been authenticated (logged in).
    * 
    * @return boolean
    */
    public function byID($ID) {
        $user = pg_fetch_array(pg_query_params($this->db, "SELECT * FROM users WHERE id=$1", array($ID)), NULL, PGSQL_ASSOC);
	unset($user['password']);
        $user['profile_image'] = 'uploads/' . $user['profile_image'];

	return $user;
    }
    
    
    /*
    * Returns the current user data as an assoc array. 
    *
    * Parameters:
    *     $publicFlag - Defaults to False, providing data directly from DB. If set to True,
    *                   output array is formatted for public display. This includes removing the hashed password
    *                   and formatting the profile_image fields.
    */
    public function getCurrentUser($publicFlag = False) {
        
        if(!$this->user && $_SESSION['user_id'] > 0) {
            $this->user = pg_fetch_array(pg_query_params($this->db, 'SELECT * FROM users WHERE id=$1', array($_SESSION['user_id'])), NULL, PGSQL_ASSOC);
        }
        
        if($publicFlag) {
            unset($this->user['password']);
            $this->user['profile_image'] = 'uploads/' . $this->user['profile_image'];
        }
        
        return $this->user;
    }
    
    
    /*
    * For pages which require user be signed in, forces application to exit with error if not signed in.
    * 
    * @return boolean
    */
    public function confirmSignedOn() {
        
        if(!$this->isSignedOn())
            jsonClientError('AUTH_NEEDED', 'You must login to access this page.');
        
        return True;
    }
}

