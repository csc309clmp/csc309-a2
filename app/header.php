<?php
session_save_path('sess');

session_start();

ini_set('display_errors', 'Off');
error_reporting(E_ERROR);

define("RESPONSE_OK", 200);
define("RESPONSE_INPUT_ERROR", 422);

$dbconn = pg_connect("host=localhost dbname=licharl2 user=licharl2 password=8962700");

function jsonOK($data) {
    response(RESPONSE_OK, NULL, NULL, $data);
}

function jsonClientError($code, $msg) {
    response(RESPONSE_INPUT_ERROR, $code, $msg, array());
}

function response($type = RESPONSE_OK, $code, $msg, $data) {
    header(' ', true, $type);
    header('Content-Type: application/json');
    
    if($code)
        $data['code'] = $code;
    
    if($msg)
        $data['message'] = $msg;
    
    echo json_encode($data);
    die();
}

function publicUserData($user) {
    unset($user['password']);
    $user['profile_image'] = 'uploads/' . $user['profile_image'];
    return $user;
}

function checkAuth() {
    global $dbconn;
    
    if(isset($_SESSION['user_id'])) {
        pg_prepare($dbconn, "get_user", 'SELECT * FROM users WHERE id=$1');
        $result = pg_num_rows(pg_execute($dbconn, "get_user", array($_SESSION['user_id'])));
        
        if($result > 0)
            return True;
    }
    jsonClientError('AUTH_NEEDED', 'You must login to access this page.');
}


require_once('TwitterAPIExchange.php');

$settings = array(
    'oauth_access_token' => "702236781996023808-87bYrB1qE8G4ALbUcHlrudxTrSDOpiJ",
    'oauth_access_token_secret' => "4mCjp3o7CzhU6RRNtwl9iwMK6E2dMlowaQ4pGcihxvxjE",
    'consumer_key' => "xpKujSxpWCKNlcQtCHLjiMTNP",
    'consumer_secret' => "xf9v3RYkko308kEOl4oLv1raCHmf9nV7zmUfT6kQAqxrN6T36V"
);

$twitter = new TwitterAPIExchange($settings);
