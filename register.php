<?php
/*
INPUTS EXPECTED:

    username - The username supplied on login form.
    password - The password supplied on login form.
    profile_img - The profile image.
    
    
JSON RESPONSE:
    {
        status: OK | ERROR
        [code]: string - The error code
        [message]: string - User readable error message.
    }
*/

require_once('app/header.php');

$username = strtolower($_POST["username"]);
$password = $_POST['password'];
$profile_image = $_FILES['profile_img'];

// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.
function better_crypt($input, $rounds = 7) {
    $salt = "";
    $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
    for($i=0; $i < 22; $i++) {
      $salt .= $salt_chars[array_rand($salt_chars)];
    }
    return crypt($input, sprintf('$2y$%02d$', $rounds) . $salt);
}

//Check username is not empty.
if(empty($username))
    jsonClientError('USERNAME_EMPTY', 'You must enter a username.');

//Check password is not empty.
if(empty($password))
    jsonClientError('PASSWORD_EMPTY', 'You must enter a password.');

//Check password length is greater or equal to 6.
if(strlen($password) < 6)
    jsonClientError('PASSWORD_TOO_SHORT', 'Your password length must be at least 6 characters long.');

//Check first charcater is alpha
if(!ctype_alpha(substr($username, 0, 1)))
    jsonClientError('USERNAME_ILLEGAL_CHARACTERS', 'Username must start with an alpha character.');

//Check is username contains illegal characters
if(!ereg('^[A-Za-z0-9_]+$', $username))
    jsonClientError('USERNAME_ILLEGAL_CHARACTERS', 'Username can only contain letters, numbers, and underscore.');

//Check if the username is already being used.
pg_prepare($dbconn, "check_user_exists", 'SELECT * FROM users WHERE username=$1');
if(pg_num_rows(pg_execute($dbconn, "check_user_exists", array($username))) >= 1) {
    jsonClientError('USERNAME_TAKEN', 'The username you entered is already taken.');
}



//Source: http://php.net/manual/en/features.file-upload.php
try {
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (!isset($profile_image['error']) || is_array($profile_image['error']))
        throw new RuntimeException('You must upload a profile image.');

    // Check $_FILES['upfile']['error'] value.
    switch ($profile_image['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }
    
    if ($profile_image['size'] > 500000) {
        throw new RuntimeException('Exceeded filesize limit.');
    }

    $ext = pathinfo($profile_image['name'], PATHINFO_EXTENSION);
    if($ext != 'jpg' && $ext != 'png' && $ext != 'gif')
        throw new RuntimeException('Image extension must be jpg, png, or gif. '. $ext);

    $profile_image_name = sha1_file($profile_image['tmp_name']) . '.' . $ext;
    if (!move_uploaded_file($profile_image['tmp_name'], sprintf('./uploads/%s', $profile_image_name)))
        throw new RuntimeException('Failed to move uploaded file.');

} catch (RuntimeException $e) {
    jsonClientError('IMAGE_ERROR', $e->getMessage());
}

pg_prepare($dbconn, "create_user", 'INSERT INTO users (username, password, profile_image) VALUES ($1, $2, $3)');
pg_execute($dbconn, "create_user", array($username, better_crypt($password), $profile_image_name));

pg_prepare($dbconn, "get_user", 'SELECT * FROM users WHERE username=$1');

$user = pg_fetch_array(pg_execute($dbconn, "get_user", array($username)), NULL, PGSQL_ASSOC);

$_SESSION['user_id'] = $user['id'];
jsonOK(publicUserData($user));
