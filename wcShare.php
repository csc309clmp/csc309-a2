<?php
/*
 * Allows current user to share their word cloud with others.
 *
 * Expects:
 *     word_cloud_id(int) - The word cloud ID the current user wants to share.
 *
 * Outputs: Either an empty success response or some error on failure.
 *          
 */
require_once('app/header.php');
require_once('app/user.php');
require_once('app/wordcloud.php');

$user = new User();
$user->confirmSignedOn();
$wordcloud = new WordCloud();

try {
    $wordcloud->share($_POST['word_cloud_id']);
} catch (Exception $e) {
    jsonClientError($e->getMessage(), 'Could not share this word cloud.');
}

jsonOK();
